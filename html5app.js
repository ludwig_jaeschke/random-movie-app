﻿var abschnitt, neueintrag, button, startnachricht, aufgeraeumt, storage, d, eingabe, autor, autor_db;
var Wochentage = new Array('Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag');
var Monate = new Array('Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember');
function $(str, nr) {return document.getElementsByTagName(str)[nr];}
function c(str) {return document.createElement(str);}
function txt(str) {return document.createTextNode(str);}
window.onload = init;

window.addEventListener('online', function() {debug('bin im Netz')}, false);
window.addEventListener('offline', function() {debug('bin offline')}, false);

function init() {
 debug('Online-Status: ' + navigator.onLine);
 cachestatus();
 abschnitt = $('section', 0);
 neueintrag = $('textarea', 0);
 button = $('input', 0);
 startnachricht = neueintrag.value;
 neueintrag.onclick = aufraeumen;
 neueintrag.onfocus = aufraeumen;
 button.onclick = speichern;
 if (typeof(localStorage) == 'undefined' || typeof(localStorage) == 'unknown') {
  debug('kein localStorage');
 } else {
  storage = true;
  lesen();
 }
}

function aufraeumen() {
 if (aufgeraeumt) return;
 neueintrag.value = '';
 aufgeraeumt = true;
}

function speichern() {
 if (!storage) return;
 eingabe = neueintrag.value;
 if (eingabe == '' || eingabe == startnachricht) return;
 var datum = new Date();
 d = datum.getTime();
 autor = $('h2', 0).firstChild.nodeValue;
 autor_db = localStorage.getItem('autor');
 if (navigator.geolocation) {
  navigator.geolocation.getCurrentPosition(adresse_ermitteln, adresse_fehler, {timeout:1500});
 } else {
  daten_schreiben();
 }
}

function daten_schreiben() {
 try {
  if (autor != autor_db) localStorage.setItem('autor', autor);
  localStorage.setItem(d, eingabe);
  debug('Eintrag lokal gespeichert');
  neueintrag.value = startnachricht;
  aufgeraeumt = false;
 } catch (e) {
  debug('Fehler beim Speichern: ' + e);
 }
 lesen();
}

function lesen() {
 if ($('ol', 0)) abschnitt.removeChild($('ol', 0));
 if (!storage) return;
 if (autor = localStorage.getItem('autor')) $('h2', 0).firstChild.nodeValue = autor;
 var ol = c('ol');
 var keys = Array();
 for (var i = 0; i < localStorage.length; i++) keys.push(localStorage.key(i));
 keys = keys.sort();
 for (var i = keys.length; i-- > 0;) {
  var schluessel = keys[i];
  var wert;
  if (!(wert = localStorage.getItem(schluessel))) continue;
  var tmp = wert.split(':ORT:', 2);
  wert = tmp[0];
  var ort = tmp[1];
  schluessel = parseInt(schluessel);
  if (isNaN(schluessel)) continue;
  var li = c('li');
  var div = c('div');
  var h3 = c('h3');
  var p = c('p');
  var dat = new Date(schluessel);
  var dat_wochentag = Wochentage[dat.getDay()];
  var dat_kalendertag = dat.getDate();
  var dat_monat = dat.getMonth();
  var dat_jahr = dat.getFullYear();
  var dat_stunde = dat.getHours();
  var dat_minute = dat.getMinutes();
  if (dat_minute < 10) dat_minute = '0' + dat_minute;
  var aenderungsdatum = dat_wochentag + ', ' + dat_kalendertag + '. ' + Monate[dat_monat] + ' ' + dat_jahr + ' um ' + dat_stunde + ':' + dat_minute;
  if (!letzte_aenderung) {
   var letzte_aenderung = aenderungsdatum;
   if (++dat_monat < 10) dat_monat = '0' + dat_monat;
   if (dat_kalendertag < 10) dat_kalendertag = '0' + dat_kalendertag;
   if (dat_stunde < 10) dat_stunde = '0' + dat_stunde;
   var letzte_aenderung_pd = dat_jahr + '-' + dat_monat + '-' + dat_kalendertag + 'T' + dat_stunde + ':' + dat_minute;
   var time = $('time', 0);
   time.firstChild.nodeValue = letzte_aenderung;
   time.setAttribute('datetime', letzte_aenderung_pd);
  }
  h3.appendChild(txt(aenderungsdatum));
  if (ort) {
   h3.appendChild(c('br'));
   h3.appendChild(txt('in ' + ort));
  }
  p.appendChild(txt(wert));
  div.appendChild(h3);
  div.appendChild(p);
  div.setAttribute('id', schluessel);
  li.appendChild(div);
  ol.appendChild(li);
 }
 if (li) abschnitt.appendChild(ol);
 var eintraege = ol.getElementsByTagName('div');
 for (var i = 0; i < eintraege.length; i++) eintraege[i].ondblclick = loeschen;
}

function loeschen() {
 if (!storage) return;
 var frage = confirm("Wollen Sie diesen Eintrag wirklich löschen?");
 if (frage) {
  localStorage.removeItem(this.id);
  debug('Eintrag lokal gelöscht');
  lesen();
 }
}

function adresse_ermitteln(pos) {
 var geo = new google.maps.Geocoder();
 var coords = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
 geo.geocode({'latLng': coords}, function(results, status) {
  var adresse = (status == google.maps.GeocoderStatus.OK)? results[0].formatted_address : 'Adresse nicht ermittelt: ' + status;
  eingabe += ':ORT:' + adresse;
  daten_schreiben();
 });
 debug('Adresse aufgelöst');
}

function adresse_fehler(err) {
 eingabe += ':ORT:Probleme bei der Ortung';
 switch(err.code) {
  case 1 : debug('Ortung: keine Erlaubnis'); break;
  case 2 : debug('Ortung: Server konnte nicht orten'); break;
  case 3 : debug('Ortung: Timeout'); break;
  default : debug('Ortung: keine Angabe'); break;
 }
 daten_schreiben();
}

function cachestatus() {
 var appcache = window.applicationCache;
 if (!appcache) return;
 appcache.addEventListener('checking', function() {debug('Prüfe den Cache ...');}, false);
 appcache.addEventListener('noupdate', function() {debug('Kein Cache-Update nötig');}, false);
 appcache.addEventListener('downloading', function() {debug('Aktualisiere den Cache ...');}, false);
 appcache.addEventListener('progress', function() {debug('Lade Datei herunter ...');}, false);
 appcache.addEventListener('updateready', function() {debug('Cache-Update bereit ...');}, false);
 appcache.addEventListener('cached', function() {debug('Cache ist aktuell');}, false);
 appcache.addEventListener('obsolete', function() {debug('Cache ist obsolet');}, false);
 appcache.addEventListener('error', function(e) {debug('Problem mit Cache: ' + e);}, false);
}

function debug(text) {
 if (!document.getElementById('debug')) {
  var div = c('div');
  div.setAttribute('id', 'debug');
  document.body.appendChild(div);
 }
 var p = c('p');
 var text = txt(text);
 p.appendChild(text);
 document.getElementById('debug').appendChild(p);
}